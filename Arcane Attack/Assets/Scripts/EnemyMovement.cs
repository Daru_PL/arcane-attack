﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public GameObject enemy;
    public GameObject Base;
    public GameObject player;

    public static int damage;
    public float speed = 3f;
    public static int hp;

    private Rigidbody2D rb;
    private Vector2 movementPlayer;
    private Vector2 movementBase;

    public static bool attacked;

	// Use this for initialization
	void Start () {
        attacked = false;

        rb = this.GetComponent<Rigidbody2D>();

        //if enemy has a certain name have a set hp and damage here.
        if (gameObject.name == "Minion")
        {
            hp = 100;
            damage = 25;

        }

	}

    void Update()
    {
        Vector3 directionPlayer = player.transform.position - transform.position;
        directionPlayer.Normalize();
        movementPlayer = directionPlayer;

        Vector3 directionBase = Base.transform.position - transform.position;
        directionBase.Normalize();
        movementBase = directionBase;


        if (hp <= 0)
        {
            Destroy(this.gameObject);

        }

    }

    void FixedUpdate()
    {
        if (attacked == false)
        {
            moveBase(movementBase);

        }

        if (attacked == true)
        {
            moveCharacter(movementPlayer);

            if (Player.currentHealth <= 0)
            {
                attacked = false;

            }
        }


    }

    void moveCharacter(Vector2 directionPlayer)
    {
        rb.MovePosition((Vector2)transform.position + (directionPlayer * speed * Time.deltaTime));

    }

    void moveBase(Vector2 directionBase)
    {
        rb.MovePosition((Vector2)transform.position + (directionBase * speed * Time.deltaTime));
    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Base")
        {
            Destroy(this.gameObject);
            BaseBehaviour.hp = BaseBehaviour.hp - 1;

            Debug.Log(BaseBehaviour.hp);
        }

        

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            attacked = true;

            

        }


    }

    

}
