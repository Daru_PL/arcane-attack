﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownController : MonoBehaviour {

    public int countdownTime;
    public Text countdownDisplay;

    public GameObject player;

    bool stop = false;
    

    void Start()
    {
        countdownDisplay.gameObject.SetActive(false);



    }

    void Update()
    {

        if (Player.currentHealth <= 0 && stop == false)
        {
            countdownDisplay.gameObject.SetActive(true);

            StartCoroutine(CountdownToStart());
            

            stop = true;

            Invoke("ExecuteAfterTime", 3);

            
            
        }
        
        if (Player.currentHealth == 100)
        {
            player.gameObject.SetActive(true);

        }

    }


    IEnumerator CountdownToStart()
    {
        


        while(countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();

            yield return new WaitForSeconds(1f);

            countdownTime--;

        }

        countdownDisplay.text = "Respawned";

        yield return new WaitForSeconds(1f);

        countdownDisplay.gameObject.SetActive(false);
    }

	public void ExecuteAfterTime()
    {
        Player.currentHealth = 100;
        

    }


}
