﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

    float timeLeft = 20.0f;
    public Text timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timeLeft -= Time.deltaTime;

        timer.text = timeLeft.ToString();

        if (timeLeft <= 0 && BaseBehaviour.hp > 0)
        {
            Debug.Log("Victory!");
            //End game
            
        }


	}
}
