﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public int maxHealth = 100;
    public static int currentHealth;
    public Healthbar healthBar;
    
    public float speed = 5f;

    public GameObject player;

    public Rigidbody2D rb;

    public CameraShake cameraShake;

    //public Camera cam;

    Vector2 movement;
    //Vector2 mousePos;

	// Use this for initialization
	void Start () {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

	}
	
	// Update is called once per frame
	void Update () {

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        if (currentHealth <= 0)
        {
            player.gameObject.SetActive(false);

        }

        if (currentHealth >= 100)
        {
            healthBar.SetHealth(currentHealth);

        }


	}

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);


        //Vector2 lookDir = mousePos - rb.position;
        //float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        //rb.rotation = angle;

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            currentHealth = currentHealth - EnemyMovement.damage;

            healthBar.SetHealth(currentHealth);
            Debug.Log("Player HP: " + currentHealth);

            StartCoroutine(cameraShake.Shake(.15f, .4f));
        }


    }

    


}
